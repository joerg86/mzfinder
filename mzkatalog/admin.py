from django.contrib import admin
from models import Kunde, Medienart, Systematik, Mimetype, Medium, Favorit, Download, Ordner, Anbieter, AnbieterListe, Notiz, Katalog

class MediaAdmin(admin.ModelAdmin):
   list_display = ("id_local", "anbieter", "title")
   list_filters = ("anbieter",)

class FavoritAdmin(admin.ModelAdmin):
   list_display = ("user", "medium", "added")

class DownloadAdmin(admin.ModelAdmin):
   list_display = ("medium", "started")

class OrdnerAdmin(admin.ModelAdmin):
   list_display = ("name", "user")

class AnbieterAdmin(admin.ModelAdmin):
   list_display = ("kunde", "name", "username", "order")
   list_editable = ("order",)

class MimetypeAdmin(admin.ModelAdmin):
   list_display = ("major", "minor", "description")

class SystematikAdmin(admin.ModelAdmin):
   list_display = ("nr", "text")

class MedienartAdmin(admin.ModelAdmin):
   list_display = ("nr", "text", "icon")
   list_editable = ("icon", )

class KundeAdmin(admin.ModelAdmin):
   list_display = ("slug", "name", "url", "adresse", "plz", "ort", "admin")


admin.site.register(Medium, MediaAdmin)
admin.site.register(Favorit, FavoritAdmin)
admin.site.register(Download, DownloadAdmin)
admin.site.register(Ordner, OrdnerAdmin)
admin.site.register(Anbieter, AnbieterAdmin)
admin.site.register(Notiz)
admin.site.register(Mimetype, MimetypeAdmin)
admin.site.register(Katalog)
admin.site.register(AnbieterListe)
admin.site.register(Systematik, SystematikAdmin)
admin.site.register(Kunde, KundeAdmin)
admin.site.register(Medienart, MedienartAdmin)
