# +-+ coding: utf-8 +-+

from django.db import models, IntegrityError
from mptt.models import MPTTModel, TreeManager, TreeForeignKey
from backends.merlin import MerlinBackend
from backends.mhybrid import MerlinHybridBackend
from jsonfield import JSONField
from backends.arix import ArixBackend
from backends.local import LocalBackend
from backends.youtube import YoutubeBackend
from django.conf import settings
import mimetypes
import os.path


BACKEND_CHOICES = (
   ("arix", "Antares via ARI/X"),
   ("merlin-hybrid", "Merlin-Hybrid (lokal & NiBis)"),
   ("merlin-xml", "NiBis-Merlin-XML"),
   ("local", "Lokale Datenbank"),
   ("youtube", "YouTube"),
)

BACKEND_CLASSES = {
   "arix": ArixBackend,
   "merlin-xml": MerlinBackend,
   "merlin-hybrid": MerlinHybridBackend,
   "local": LocalBackend,
   "youtube": YoutubeBackend,
}

# Create your models here.
class Anbieter(models.Model):
   name = models.CharField(max_length=50)
   backend = models.CharField("Schnittstelle", max_length=50, choices=BACKEND_CHOICES)
   url = models.URLField("URL", blank=True, null=True)
   username = models.CharField("Benutzername", max_length=200, blank=True, null=True)
   password = models.CharField("Passwort", max_length=200, blank=True, null=True)
   config = JSONField("Erweiterte Einstellungen (JSON)", blank=True, null=True)
   order = models.PositiveIntegerField("Reihenfolge", default=0)
   logo = models.ImageField(upload_to="img/provider_logos", blank=True, null=True, default=None)
   show = models.BooleanField("Anzeigen", help_text="Standardmäßig bei allen Nutzern anzeigen.", default=True)
   invisible = models.BooleanField("Unsichtbar", help_text="Der Anbieter wird nicht in der Suchmaschine angezeigt oder zum Hinzufügen angeboten", default=False)
   user_warning = models.TextField("Benutzer-Warnung", help_text="Warnmeldung mit Hinweis auf Nutzungsbedingungen, Urheberrecht etc.", blank=True, null=True)
   kunde = models.ForeignKey("Kunde", null=True, blank=True)

   def get_backend(self):
      return BACKEND_CLASSES[self.backend](self, Medium, self.url, self.username, self.password, self.config)

   def __unicode__(self):
      return self.name

   class Meta:
      ordering = ("order", "name")
      verbose_name = "Anbieter"
      verbose_name_plural = "Anbieter"


class Kunde(models.Model):
    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=100)
    short = models.CharField(max_length=100)
    url = models.URLField(blank=True,null=True)
    adresse = models.CharField("Adresse", max_length=100, null=True, blank=True)
    adresse2 = models.CharField("Adresse (2. Zeile)", max_length=100, null=True, blank=True)
    plz = models.CharField("PLZ", max_length=5, null=True, blank=True)
    ort = models.CharField("Ort", max_length=100, null=True, blank=True)
    admin = models.ForeignKey("auth.User", null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Kunde"
        verbose_name_plural = "Kunden"

class AnbieterListe(models.Model):
   anbieter = models.ForeignKey("Anbieter")
   user = models.ForeignKey("auth.User")
   order = models.PositiveIntegerField()

   class Meta:
      verbose_name = "Anbieter-Liste"
      verbose_name_plural = "Anbieter-Listen"
      ordering = ("order", )
      unique_together = ("anbieter", "user")
   

class Katalog(models.Model):
   name = models.CharField(max_length=100)
   url = models.URLField(blank=True, null=True)
   logo_url = models.URLField(blank=True, null=True)

   def __unicode__(self):
      return self.name

   class Meta:
      verbose_name = "Katalog"
      verbose_name_plural = "Kataloge"
   
class Mimetype(models.Model):
   major = models.CharField(max_length=100)
   minor = models.CharField(max_length=100, blank=True, null=True)
   description = models.CharField(max_length=100)
   icon = models.ImageField(upload_to="img/mimetypes/16")

   def __unicode__(self):
      return "%s/%s" % (self.major, self.minor)

   class Meta:
      unique_together = ("major", "minor")
   

class Medium(models.Model):
   anbieter = models.ForeignKey("Anbieter")
   katalog = models.ForeignKey("Katalog", blank=True, null=True)
   id_local = models.CharField("Lokale ID", max_length=255)
   title = models.CharField("Titel", max_length=255, blank=True, null=True)
   description = models.TextField("Beschreibung", blank=True, null=True)
   datasheet = models.URLField(blank=True,null=True, max_length=255)
   thumbnail = models.URLField(blank=True,null=True, max_length=255)
   animated = models.URLField(blank=True,null=True, max_length=255)
   logo = models.URLField(blank=True, null=True)
   protected = models.BooleanField(default=True)
   parent = models.ForeignKey("Medium", null=True, blank=True)
   subitem_cnt = models.PositiveIntegerField(null=True, blank=True)

   download = models.FileField(blank=True, null=True, upload_to="cache")
   media_url = models.URLField("Medien-URL", blank=True, null=True)
   path = models.CharField(max_length=255, blank=True, null=True) # suggested path on disk

   bookmarkers = models.ManyToManyField("auth.User", through="Favorit")

   downloadable = models.BooleanField(default=False)
   playable = models.BooleanField(default=True)

   owner = models.ForeignKey("auth.User", null=True, blank=True, related_name="own_media_set")
   search_additional = models.TextField("Zusatzinfos für Suchmaschine", null=True, blank=True)
   systematik = models.ManyToManyField("Systematik", blank=True)
   medienart = models.ForeignKey("Medienart", null=True, blank=True)

   @property
   def title_or_basename(self):
      if self.title: return self.title
      if not self.title and self.id_local:
         return os.path.basename(self.id_local)
   

   @property
   def mimetype(self):
      if self.path:
         t, e = mimetypes.guess_type(self.path)
         if t:
            major, minor = t.split("/", 1)
            try:
               m = Mimetype.objects.get(major=major,minor=minor)
               return m
            except Mimetype.DoesNotExist:
               try:
                  m = Mimetype.objects.get(major=major,minor="")
                  return m
               except Mimetype.DoesNotExist:
                  return None
               
            


   def schedule_download(self):
      try:
         d = Download.objects.get(medium=self)
      except Download.DoesNotExist:
         pass
      else:
         if d.task_id:
            if download.AsyncResult(d.task_id).ready():
               d.delete()

      try:
         d = Download.objects.create(medium=self)
      except IntegrityError:
         # already downloading
         return False
      else:
         d.task_id = download.delay(self)
         d.save()

   def __unicode__(self):
      return self.id_local

   class Meta:
      verbose_name = "Medium"
      verbose_name_plural = "Medien"
      unique_together = ("id_local", "anbieter")

class Medienart(models.Model):
   nr = models.CharField(max_length=11, unique=True)
   text = models.CharField(max_length=100)
   icon = models.CharField(null=True, blank=True, max_length=50)

   class Meta:
      verbose_name = "Medienart"
      verbose_name_plural = "Medienarten"
      ordering = ("nr", )

   def __unicode__(self):
      return "%s %s" % (self.nr, self.text)
         


class Systematik(MPTTModel):
   nr = models.CharField(max_length=11, unique=True)
   text = models.CharField(max_length=100)
   parent = TreeForeignKey("self", null=True, blank=True, related_name="children")
   tree = TreeManager()
   objects = TreeManager()

   def __unicode__(self):
      return "%s %s" % (self.nr, self.text)

   class Meta:
      verbose_name = "Systematik"
      verbose_name_plural = "Systematik"


def get_media_status(medium):
   status = {
      "downloaded": medium.download,
   }

   try:
      downloading = medium.downloading,
   except Download.DoesNotExist:
      status["downloading"] = False
   else:
      status["downloading"] = True

   return status


class Download(models.Model):
   medium = models.OneToOneField(Medium, related_name="downloading")
   task_id = models.CharField(max_length=255, unique=True)
   started = models.DateTimeField(auto_now_add=True)

   def async_result(self):
      return download.AsyncResult(self.task_id)

   def __unicode__(self):
      return self.medium.id_local

class OrdnerManager(TreeManager):
   def get_root_for(self, user):
      root, c = self.get_or_create(user=user, parent=None, defaults={"name": "Meine Medien"})
      return root

class Notiz(models.Model):
   ordner = models.ForeignKey("Ordner")
   text = models.TextField()
   css_class = models.CharField(max_length=60)
   pos_x = models.PositiveIntegerField(default=0)
   pos_y = models.PositiveIntegerField(default=0)
   
   def __unicode__(self):
      return self.text

   class Meta:
      verbose_name = "Notiz"
      verbose_name_plural = "Notizen"

class Ordner(MPTTModel):
   name = models.CharField(max_length=200)
   user = models.ForeignKey("auth.User")
   parent = TreeForeignKey("self", null=True, blank=True, related_name="children")
   objects = OrdnerManager()
   tree = OrdnerManager()

   def get_item_count(self):
      return len(self.get_children()) + len(self.favorit_set.all())

   def __unicode__(self):
      return self.name

   class MPTTMeta:
      order_insertion_by = ["name"]

   class Meta:
      verbose_name_plural = "Ordner"
      verbose_name = "Ordner"
      unique_together = ("user", "name", "parent")
      ordering = ("name",)

class Favorit(models.Model):
   user = models.ForeignKey("auth.User")
   medium = models.ForeignKey(Medium)
   added = models.DateField(auto_now_add=True)
   order = models.IntegerField(default=0)
   ordner = models.ForeignKey(Ordner)
   

   def __unicode__(self):
      return self.user.username
   
   class Meta:
      unique_together = ("user", "medium", "ordner")
      verbose_name_plural = "Favoriten"
