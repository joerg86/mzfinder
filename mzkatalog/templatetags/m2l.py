from django import template
from django.conf import settings
from merlin2lan.proxy.models import get_media_status

register = template.Library()

def get_media_status(context, medium):
   if "merlin2lan.cloud" in settings.INSTALLED_APPS:
      from merlin2lan.cloud.models import get_media_status as gms
      return gms(medium, context.get("user"))
   else:
      return get_media_status(medium)

register.assignment_tag(takes_context=True)(get_media_status)
