#+-+ coding: utf-8 +-+

from django import forms
from django.core.files.base import ContentFile
from merlin2lan.elixier.models import Typ, Bildungsebene, Fach
from models import Anbieter, Ordner, Notiz, Medium
from pydub import AudioSegment
import mimetypes
import ffvideo
import tempfile
import os
from PIL import Image
from wand.image import Image as WandImage
from wand.exceptions import MissingDelegateError

from StringIO import StringIO

ERG_CHOICES = (
   (5, "5"),
   (10, "10"),
   (20, "20"),
   (30, "30"),
   (50, "50"),
   (100, "100"),
)
EMPTY_CHOICE = [("", "------")]
FACH_CHOICES = EMPTY_CHOICE + [(x.label, x.label) for x in Fach.objects.all()]
TYP_CHOICES = EMPTY_CHOICE + [(x.label, x.label) for x in Typ.objects.all()]
BE_CHOICES = EMPTY_CHOICE + [(x.label, x.label) for x in Bildungsebene.objects.all()]

class UnvalidatedMultipleChoiceField(forms.MultipleChoiceField):
   """Same as a MultipleChoiceField, just that it still validates if the value is NOT in the choices list."""

   def validate(self, value):
      """
      Validates that the input is a list or tuple.
      """
      if self.required and not value:
         raise ValidationError(self.error_messages['required'])
 

class SearchForm(forms.Form):
   anbieter = forms.ModelChoiceField(queryset=Anbieter.objects.all(), required=False)
   stichwort = forms.CharField(required=False)
   titel = forms.CharField(required=False)
   bildungsebene = forms.CharField(widget=forms.Select(choices=BE_CHOICES), required=False)
   fachsach = forms.CharField(label="Fach",widget=forms.Select(choices=FACH_CHOICES), required=False)
   lernressourcentyp = UnvalidatedMultipleChoiceField(label="Lernrressource",choices=TYP_CHOICES, required=False)
   mediennummer = forms.CharField(required=False)
   anzahl = forms.ChoiceField(label="Ergebnisse (pro Seite)", choices=ERG_CHOICES, initial=20, required=False)
   start = forms.IntegerField(widget=forms.HiddenInput, min_value=0, initial=0, required=False)
   narrow_systematik = forms.CharField(widget=forms.HiddenInput, required=False)

class MediaForm(forms.ModelForm):
   title = forms.CharField(label="Titel", required=True, max_length=255)
   media_url = forms.URLField(label="URL", required=True, max_length=255)
   error_css_class = "error"
   class Meta:
      model = Medium
      fields = ["media_url", "title", "description"]

class MediaFileField(forms.FileField):
   ALLOWED_TYPES = [ 
      "image/jpeg", "image/png", "image/gif", 
      "application/pdf", 
      "video/mp4", "video/ogg", "video/mpeg", 
      "audio/mpeg", "audio/ogg"
   ] 
   @staticmethod
   def get_uploaded_path(uf):
      if hasattr(uf, "temporary_file_path"):
         return uf.temporary_file_path(), False
      else:
         fd, path = tempfile.mkstemp()
         f = os.fdopen(fd, "wb")
         f.write(uf.read())
         f.close()
         return path, True


   def clean(self, *args, **kwargs):
      data = super(MediaFileField, self).clean(*args, **kwargs)
      t, enc = mimetypes.guess_type(data.name)
      if not t in MediaFileField.ALLOWED_TYPES:
         raise forms.ValidationError("Der hochgeladene Dateityp ist nicht erlaubt.")

      if t.startswith("video/"):
         path, needs_deletion = MediaFileField.get_uploaded_path(data)
         try:
            vs = ffvideo.VideoStream(path)
         except ffvideo.DecoderError:
            raise forms.ValidationError("Die Datei enthält kein gültiges Video.")
         sec = min(vs.duration * 0.5, 3)
         frame = vs.get_frame_at_sec(sec)

         sio = StringIO()
         frame.image().save(sio, format="JPEG")
         data.thumb = ContentFile(sio.getvalue())
         if needs_deletion:
            os.unlink(path)
      elif t.startswith("image/"):
         try:
            image = Image.open(data)
         except IOError:
            raise forms.ValidationError("Die Datei enthält keine gültigen Bilddaten.")
         else:
            data.thumb = data
      elif t.startswith("audio/"):
         path, needs_deletion = MediaFileField.get_uploaded_path(data)
         try:
            song = AudioSegment.from_file(path)
         except EOFError:
            raise forms.ValidationError("Die Datei enthält keine gültigen Audiodaten.")
         finally:
            if needs_deletion:
               os.unlink(path)
      elif t == "application/pdf":
         path, needs_deletion = MediaFileField.get_uploaded_path(data)
         try:
            with WandImage(filename=path+"[0]") as img:
               if img.format != "PDF":
                  raise forms.ValidationError("Kein gültiges PDF-Dokument.")
               img.format = "jpeg"
               data.thumb = ContentFile(img.make_blob())
         except MissingDelegateError:
            raise forms.ValidationError("Kein gültiges PDF-Dokument.")
         finally:
            if needs_deletion:
               os.unlink(path)
            

         

      return data


class UploadMediaForm(forms.ModelForm):
   title = forms.CharField(label="Titel", required=True, max_length=255)
   datei = MediaFileField(help_text="Erlaubte Formate: JPEG, PNG, GIF, MPEG, MP4, OGV, MP3, OGG, PDF")
   error_css_class = "error"
   class Meta:
      model = Medium
      fields = ["title", "description"]
      


class NotizForm(forms.ModelForm):
   class Meta:
      model = Notiz
      exclude = ("pos_x", "pos_y")

class NotizPosForm(forms.ModelForm):
   class Meta:
      model = Notiz
      fields = ("pos_x", "pos_y")
