from urllib import urlencode
import urllib2
from django.conf import settings
import re
import gzip

try:
   from cStringIO import StringIO
except ImportError:
   from StringIO import StringIO


LOCAL_IPS = (
   r"^127\.\d{1,3}\.\d{1,3}\.\d{1,3}$",
   r"^10\.\d{1,3}\.\d{1,3}\.\d{1,3}$",
   r"^192\.168\.\d{1,3}\.\d{1,3}$",
   r"^172\.(1[6-9]|2[0-9]|3[0-1])\.[0-9]{1,3}\.[0-9]{1,3}$",
)

def is_local_ip(ip):
   for rex in LOCAL_IPS:
      if re.match(rex, ip): return True
   return False

def is_local_request(request):
   """Find out if a request comes from the internal network."""
   if "HTTP_X_FORWARDED_FOR" in request.META:
      return is_local_ip(request.META["HTTP_X_FORWARDED_FOR"])
   elif "REMOTE_ADDR" in request.META:
      return is_local_ip(request.META["REMOTE_ADDR"])

def gzip_urlopen(url, data):
   """GZip aware wrapper around urllib2.urlopen. Returns a file-like object."""
   req = urllib2.Request(url, data)
   req.add_header("Accept-Encoding", "gzip")
   r = urllib2.urlopen(req)

   if r.info().get("Content-Encoding") == "gzip":
      r = gzip.GzipFile(fileobj=StringIO(r.read()))
   return r
      
