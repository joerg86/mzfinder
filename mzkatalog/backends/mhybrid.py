from mzkatalog.backends.merlin import MerlinBackend
from mzkatalog.backends.local import LocalBackend

class MerlinHybridBackend(MerlinBackend):
    query = LocalBackend.__dict__["query"]
