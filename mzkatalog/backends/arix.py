# +-+ coding: utf-8 +-+

from django.db import IntegrityError
from django.utils.http import urlquote as quote
from django.utils.http import urlencode
from mzkatalog.util import gzip_urlopen
from django.conf import settings
from urlparse import urlparse
from urllib import unquote
from glob import glob
import os.path
import mimetypes

try:
   from lxml.etre import ElementTree
except ImportError:
   from xml.etree import ElementTree

from htmlentitydefs import entitydefs
import urllib2

try:
   import cjson as json
except ImportError:
   import json
else:
   json.dumps = json.encode
   json.loads = json.decode

import re

class QueryResult(object):
   pass

class ArixBackend(object):
   def __init__(self, anbieter, model, url, username, password, config):
      self.anbieter = anbieter
      self.model = model
      self.url = url
      self.username = username
      self.password = password
      self.config = config

   def get_protected_url(self, m):
      """Get the real download url for a medium."""
      pdata = {}
      pdata["cmd"] = True
      pdata["username"] = self.username
      pdata["password"] = self.password
      pdata = urlencode(pdata)

      r = urllib2.urlopen(m.media_url, pdata)
      downurl = r.geturl()
      if downurl == m.media_url:
         return None
      else:
         if "&format=default-mp4" in m.media_url and not "/MP4-DEFAULT/" in downurl:
            root, ext = os.path.splitext(downurl)
            downurl = root.replace("/FILE/", "/MP4-DEFAULT/") + ".mp4"

         return downurl

   def get_etree_for_req(self, r):
      """Parse a file-object's content into an ElementTree"""
      parser = ElementTree.XMLParser()
      parser.entity.update(entitydefs)
      data = r.read()
      parser.feed(data)
      et = parser.close()
      return et

   def get_facets_from_xml(self, et):
      res = {}
      node = et.find("ressources")
      if node != None:
         res["lernressourcentyp"] = []
         lrt = res["lernressourcentyp"]
         for f in node.findall("data"):
            try:
               count = int(f.find("sum").text)
            except ValueError:
               count = 0
            value = f.find("name").text
            if value:
               lrt.append((value, count))
         
      """ Altes facets-Tag
      if node != None:
         for f in node.findall("field"):
            name = f.attrib.get("name")
            if name:
               res[name] = []
               for token in f.findall("token"):
                  value = token.attrib.get("value")
                  try:
                     count = int(token.attrib.get("count", "0"))
                  except ValueErro:
                     count = 0
                  if value:
                     res[name].append((value, count))
      """
      return res
      

   def get_item_from_xml(self, item, commit=True, anbieter=None):
      if anbieter is None:
         anbieter = self.anbieter

      id_local = item.get("identifier")

      defaults = {
        "title": item.findtext("f[@n='titel']"),
        "description": item.findtext("f[@n='text']"),
        "thumbnail": "https://cover.datenbank-bildungsmedien.net/100x75/%s.png" % id_local,
        "datasheet": "%s/record?src=online&id=%s" % (self.config["frontend_url"], id_local),
        "media_url": "%s/record?src=online&id=%s" % (self.config["frontend_url"], id_local),
      }
      mi, created  = self.model.objects.get_or_create(anbieter=anbieter, id_local=item.get("identifier"), defaults=defaults)
      
      #mi.thumbnail = item.findtext("thumbnail")
      #mi.animated = item.findtext("animated")
      return mi


   def get_items_from_xml(self, et):
      """Return a list with Mediums parsed from the XML tree."""
      result = []
      for item in et.findall("r"):
         mi = self.get_item_from_xml(item)
         result.append(mi)
      return result

   def fetch_by_id_local(self, identifier):
      url = self.url + "?explicit=1&mediennummer="+ quote(identifier)
      r = gzip_urlopen(url, "anzahl=300")
      et = self.get_etree_for_req(r)
      items = self.get_items_from_xml(et)
      return items


   def query(self, formdata):
      rawdata = formdata

      limit = "%d,%d" % (formdata.get("offset", 0), formdata.get("limit", 20))

      req = ElementTree.Element("search",  {"fields": "titel,typ,text", "limit": limit})
      cond = ElementTree.SubElement(req, "condition", {"field": "titel_fields"})
      cond.text=rawdata["stichwort"]

      data = {
         "xmlstatement": ElementTree.tostring(req, encoding="utf-8"),
      }
      postdata = urlencode(data, True)
        
      r = gzip_urlopen(self.url, postdata)
      et = self.get_etree_for_req(r)

      count = len(et.findall("r"))
      start, end, anzahl = 0, count, count


      result = self.get_items_from_xml(et)
     
      o = QueryResult() 
      o.items = result
      o.start = start
      o.end = end
      o.count = anzahl
      o.total = count
      o.facets = None
      return o

