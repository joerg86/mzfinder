# +-+ coding: utf-8 +-+

from django.db import IntegrityError
from django.utils.http import urlquote as quote
from django.utils.http import urlencode
from mzkatalog.util import gzip_urlopen

try:
   from lxml.etre import ElementTree
except ImportError:
   from xml.etree import ElementTree

from htmlentitydefs import entitydefs
import urllib2

try:
   import cjson as json
except ImportError:
   import json
else:
   json.dumps = json.encode
   json.loads = json.decode

import re
import feedparser

class QueryResult(object):
   pass

class YoutubeBackend(object):
   def __init__(self, anbieter, model, url, username, password, config):
      self.anbieter = anbieter
      self.model = model

   def get_protected_url(self, m):
      """Get the real download url for a medium."""
      pdata = {}
      pdata["cmd"] = True
      pdata["username"] = self.username
      pdata["password"] = self.password
      pdata = urlencode(pdata)

      r = urllib2.urlopen(m.media_url, pdata)
      downurl = r.geturl()
      if downurl == m.media_url:
         return None
      else:
         return downurl

   def fetch_by_id_local(self, identifier):
      url = self.url + "?explicit=1&mediennummer="+ quote(identifier)
      r = gzip_urlopen(url, "anzahl=300")
      et = self.get_etree_for_req(r)
      items = self.get_items_from_xml(et)
      return items


   def query(self, formdata):

      """
      facets = self.get_facets_from_xml(et)

      # add the display names of the fields to the facets
      disp_map = { "fachsach": "Fach-/Sachgebiet" }
      for key in facets.keys():
         if key in disp_map:
            display = disp_map[key]
         else:
            display = key.capitalize()

         facets[key, display] = facets[key]
         del facets[key]
      """
      q = formdata.get("stichwort")
      start = 0 if not formdata["start"] else formdata["start"]
      anzahl = 10 if not formdata["anzahl"] else formdata["anzahl"]

      feed = feedparser.parse(
      
         "https://gdata.youtube.com/feeds/api/videos?v=1" +
         "&q=" + quote(q) +
         "&start-index=" + str(start+1) +
         "&max-results=" + str(anzahl)
      )
      
      rawresult = feed.entries
      total = int(feed.feed.opensearch_totalresults)
      end = start + len(rawresult)

      result = []
      for r in rawresult:
         m, created = self.model.objects.get_or_create(id_local=r.id,anbieter=self.anbieter)
         m.title = r.title
         if r.description:
            m.description = r.description
         else:
            m.description = None
         m.protected = False
         m.downloadable = False
         m.media_url = r.link
         m.thumbnail = r.media_thumbnail[-1]["url"]
         m.save()
         result.append(m)

      o = QueryResult() 
      o.items = result
      o.start = start
      o.end = end
      o.count = len(result)
      o.total = total
      o.facets = None
      return o

