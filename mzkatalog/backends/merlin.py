# +-+ coding: utf-8 +-+

from django.db import IntegrityError
from django.utils.http import urlquote as quote
from django.utils.http import urlencode
from mzkatalog.util import gzip_urlopen
from django.conf import settings
from urlparse import urlparse
from urllib import unquote
from glob import glob
import os.path
import mimetypes

try:
   from lxml.etre import ElementTree
except ImportError:
   from xml.etree import ElementTree

from htmlentitydefs import entitydefs
import urllib2

try:
   import cjson as json
except ImportError:
   import json
else:
   json.dumps = json.encode
   json.loads = json.decode

import re

class QueryResult(object):
   pass

class MerlinBackend(object):
   def __init__(self, anbieter, model, url, username, password, config):
      self.anbieter = anbieter
      self.model = model
      self.url = url
      self.username = username
      self.password = password
      self.config = config

   def get_protected_url(self, m):
      """Get the real download url for a medium."""
      pdata = {}
      pdata["cmd"] = True
      pdata["username"] = self.username
      pdata["password"] = self.password
      pdata = urlencode(pdata)

      r = urllib2.urlopen(m.media_url, pdata)
      downurl = r.geturl()
      if downurl == m.media_url:
         return None
      else:
         if "&format=default-mp4" in m.media_url and not "/MP4-DEFAULT/" in downurl:
            root, ext = os.path.splitext(downurl)
            downurl = root.replace("/FILE/", "/MP4-DEFAULT/") + ".mp4"

         return downurl

   def get_etree_for_req(self, r):
      """Parse a file-object's content into an ElementTree"""
      parser = ElementTree.XMLParser()
      parser.entity.update(entitydefs)
      data = r.read()
      parser.feed(data)
      et = parser.close()
      return et

   def get_facets_from_xml(self, et):
      res = {}
      node = et.find("ressources")
      if node != None:
         res["lernressourcentyp"] = []
         lrt = res["lernressourcentyp"]
         for f in node.findall("data"):
            try:
               count = int(f.find("sum").text)
            except ValueError:
               count = 0
            value = f.find("name").text
            if value:
               lrt.append((value, count))
         
      """ Altes facets-Tag
      if node != None:
         for f in node.findall("field"):
            name = f.attrib.get("name")
            if name:
               res[name] = []
               for token in f.findall("token"):
                  value = token.attrib.get("value")
                  try:
                     count = int(token.attrib.get("count", "0"))
                  except ValueErro:
                     count = 0
                  if value:
                     res[name].append((value, count))
      """
      return { "fields": res }
      

   def get_item_from_xml(self, item, commit=True, anbieter=None):
      if anbieter is None:
         anbieter = self.anbieter
      mi, created  = self.model.objects.get_or_create(id_local=item.findtext("identifier"), anbieter=anbieter)
      mi.title = item.findtext("title")
      mi.description = item.findtext("description")
      mi.thumbnail = item.findtext("thumbnail")
      mi.animated = item.findtext("animated")

      mi.datasheet = "https://search.merlin.nibis.de/einzelinfo.php?id_local=" + quote(mi.id_local)
      mi.logo = item.findtext("logo")

      if mi.logo and not "://" in mi.logo:
         mi.logo = "http://search.merlin.nibis.de/" + mi.logo
      mi.path = item.findtext("path")

      mi.parent_identifier = item.findtext("parent_identifier")

      if mi.parent_identifier:
         parent, c = self.model.objects.get_or_create(id_local=mi.parent_identifier, anbieter=self.anbieter)
         mi.parent = parent

      subs = item.findtext("item_count")
      if subs:
         try:
            mi.subitem_cnt = int(subs)
         except ValueError:
            pass

      is_merlin = (item.findtext("is_merlin") in ["1", "true", "yes"])
      if is_merlin:
         mi.protected = True
      else:
         mi.protected = False

      # NiBis Merlin: Only merlin subitems are downloadable
      if is_merlin and mi.parent:
         mi.downloadable = True
         mi.playable = True
      elif is_merlin:
         mi.downloadable = False
         mi.playable = False
      else:
         mi.downloadable = False
         mi.playable = True

      # Other: check the flag
      dl = item.findtext("downloadable")
      if dl:
         mi.downloadable = (dl in ["1", "true", "yes" ])

      pl = item.findtext("playable")
      if pl:
         mi.playable = (not pl in ["0", "false", "no" ])


      mi.media_url = item.findtext("media_url")
      if mi.media_url and not "://" in mi.media_url:
         mi.media_url = "http://search.merlin.nibis.de/" + mi.media_url

      if commit:
         mi.save()
      return mi


   def get_items_from_xml(self, et):
      """Return a list with Mediums parsed from the XML tree."""
      result = []
      for item in et.findall("./items/data"):
         mi = self.get_item_from_xml(item)
         result.append(mi)
      return result

   def fetch_by_id_local(self, identifier):
      url = self.url + "?explicit=1&mediennummer="+ quote(identifier)
      r = gzip_urlopen(url, "anzahl=300")
      et = self.get_etree_for_req(r)
      items = self.get_items_from_xml(et)
      return items


   def query(self, formdata):
      rawdata = formdata
      data = {
         "sortings[]": [ "is_merlin", "parent_identifier", "id_local" ],
         "orders[]": [ "desc", "asc", "asc" ],
         "db": "global",
      }

      data["stichwort"] = formdata["stichwort"]
      data["start"] = formdata.get("offset", 0)
      data["anzahl"] = formdata.get("limit", 20)

      if "kreis" in self.config:
         data["kreis"] = self.config["kreis"]

      postdata = urlencode(data, True)
         
      r = gzip_urlopen(self.url, postdata)
      et = self.get_etree_for_req(r)
      count = int(et.findtext("sum"))
      if count:
         start = int(et.findtext("start"))
         end = int(et.findtext("sumAnzahl"))
         anzahl = int(et.findtext("anzahl"))
      else:
         start, end, anzahl = 0, 0, 0

      facets = self.get_facets_from_xml(et)

      # add the display names of the fields to the facets
      """
      disp_map = { "fachsach": "Fach-/Sachgebiet" }
      for key in facets["fields"].keys():
         if key in disp_map:
            display = disp_map[key]
         else:
            display = key.capitalize()

         facets["fields"][key, display] = facets["fields"][key]
         del facets["fields"][key]
      """

      result = self.get_items_from_xml(et)
     
      o = QueryResult() 
      o.items = result
      o.start = start
      o.end = end
      o.count = anzahl
      o.total = count
      o.facets = facets
      o.facets = None
      return o

