# +-+ coding: utf-8 +-+

from django.db import IntegrityError
from django.utils.http import urlquote as quote
from django.utils.http import urlencode
from mzkatalog.util import gzip_urlopen
#from cloud.models import Upload

try:
   from lxml.etre import ElementTree
except ImportError:
   from xml.etree import ElementTree

from htmlentitydefs import entitydefs
import urllib2

try:
   import cjson as json
except ImportError:
   import json
else:
   json.dumps = json.encode
   json.loads = json.decode

import re

class QueryResult(object):
   pass

class LocalBackend(object):
   def __init__(self, anbieter, model, url, username, password, config):
      self.anbieter = anbieter
      self.model = model
      self.config = config
      if not self.config:
         self.config = {}

   def get_protected_url(self, m):
      """Get the real download url for a medium."""
      try:
         u = Upload.objects.get(medium=m)
      except Upload.DoesNotExist:
         return u.media_url 
      else:
         return u.datei.url
      
      

   def fetch_by_id_local(self, identifier):
      url = self.url + "?explicit=1&mediennummer="+ quote(identifier)
      r = gzip_urlopen(url, "anzahl=300")
      et = self.get_etree_for_req(r)
      items = self.get_items_from_xml(et)
      return items


   def query(self, formdata):

      """
      facets = self.get_facets_from_xml(et)

      # add the display names of the fields to the facets
      disp_map = { "fachsach": "Fach-/Sachgebiet" }
      for key in facets.keys():
         if key in disp_map:
            display = disp_map[key]
         else:
            display = key.capitalize()

         facets[key, display] = facets[key]
         del facets[key]
      """
      from haystack.query import SearchQuerySet
      sqs = SearchQuerySet()
      
      fullresult = sqs.filter(anbieter_id__exact=self.anbieter.id).auto_query(formdata.get("stichwort"))
      fc = fullresult.facet("systematik", mincount=1).facet("medienart", mincount=1).facet_counts()

      if formdata["systematik"]:
         fullresult = fullresult.filter(systematik__in=formdata["systematik"])
      if formdata["medienart"]:
         fullresult = fullresult.filter(medienart__in=formdata["medienart"])


      start = 0 if not formdata["offset"] else formdata["offset"]

      anzahl = 10 if not formdata["limit"] else formdata["limit"]
      end = min(len(fullresult), start + anzahl)

      result = [x.object for x in fullresult[start:end]]
     
      o = QueryResult() 
      o.items = result
      o.start = start
      o.end = end
      o.count = anzahl
      o.total = len(fullresult)
      o.facets = fc
      return o

