# +-+ coding: utf-8 +-+

from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.files.base import File 
from django.core.urlresolvers import reverse
from django.conf import settings
from django.template.context import RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import ListView, TemplateView
from django.utils.decorators import method_decorator
from django.template.loader import render_to_string
from django.db import IntegrityError
from models import Medium, Favorit, Download, Ordner, Anbieter, AnbieterListe, Notiz, Systematik
from forms import MediaForm, UploadMediaForm
from util import is_local_request, gzip_urlopen
from urllib import quote, urlencode
from django.utils.http import urlquote as quote
from django.utils.http import urlencode
from tempfile import NamedTemporaryFile 
import feedparser
import datetime
from time import mktime

try:
   from lxml.etre import ElementTree
except ImportError:
   from xml.etree import ElementTree

from forms import SearchForm, NotizForm, NotizPosForm
from htmlentitydefs import entitydefs
import urllib2
from haystack.query import SearchQuerySet

try:
   import cjson as json
except ImportError:
   import json
else:
   json.dumps = json.encode
   json.loads = json.decode

import re

if "merlin2lan.cloud" in settings.INSTALLED_APPS:
   from merlin2lan.cloud.models import Organisation
   from merlin2lan.cloud.models import Download as CloudDownload
   from merlin2lan.cloud.models import Upload

# Create your views here.
class AddNoteView(TemplateView):
   template_name = "proxy/mymedia_add_note.html"

class AddMediaView(TemplateView):
   template_name = "proxy/mymedia_add_media.html"

@login_required
def add_media_link(request, upload=False):
   if "merlin2lan.cloud" in settings.INSTALLED_APPS:
      org = Organisation.find_by_user(request.user)
   else:
      org = None

   if not upload:
      form_class = MediaForm
   else:
      form_class = UploadMediaForm

   # Anbieter für User Generated Content
   anbieter = None
   for a in Anbieter.objects.filter(backend="local"):
      config = a.get_backend().config
      if "user_generated_content" in config and config["user_generated_content"]:
         anbieter = a
         break
   if not anbieter:
      raise PermissionDenied()

   # Favoriten-Ordner holen
   fid = None
   if fid != None:
      folder = get_object_or_404(Ordner, pk=fid, user=request.user)
   else:
      folder = Ordner.objects.get_root_for(request.user)



   if request.method == "POST":
      form = form_class(request.POST, request.FILES)
      if form.is_valid():
         # Form ist in Ordnung, zusätzliche Felder füllen und Medium speichern
         i = form.save(commit=False)

         if upload:
            f = request.FILES["datei"]
            u = Upload(user=request.user)
            u.datei.save(f.name, f)

            if hasattr(f, "thumb"):
               u.thumbnail.save(f.name + "_thumb.jpg", f.thumb)

            i.media_url = u.datei.url
            i.id_local = i.media_url
         else:
            u = None

         i.owner = request.user
         i.anbieter = anbieter
         i.id_local = i.media_url
         i.logo = settings.MEDIA_URL + "img/mzo-auge.png"
         i.downloadable = upload
         i.protected = upload
         i.save()
         
         if u:
            u.medium = i
            u.save()

         i.id_local = i.id
         i.media_url = u.datei.url
         i.path = unicode(u.datei)
         if u.thumbnail:
            i.thumbnail = u.thumbnail["media"].url

         i.save()

         if org and i.downloadable:
            org.schedule_download(i)

         # Medium automatisch zu den Favoriten des Benutzers hinzufügen
         f, created = Favorit.objects.get_or_create(user=request.user,medium=i, ordner=folder)

         return HttpResponse(json.dumps({"location": reverse("m2l_mymedia")}), mimetype="application/json")
   else:
      form = form_class()

   cont = {
      "form": form,
      "upload": upload,
   }
   if False and upload and request.method == "POST":
      # Sonderfall jquery-file-upload
      res = {
         "html": render_to_string("proxy/mymedia_add_media_link.html", cont, RequestContext(request))
      }
      return HttpResponse(json.dumps(res), mimetype="application/json")
   else:
      return render_to_response("proxy/mymedia_add_media_link.html", cont, RequestContext(request))

def get_feed():
   feed = feedparser.parse("http://www.medienzentrum-osnabrueck.de/feed/")
   for e in feed.entries:
      if "published_parsed" in e:
         e.published_datetime = datetime.datetime.fromtimestamp(mktime(e.published_parsed))
   return feed

def welcome(request):
   cont = {
      "feed": get_feed(),
   }
   return render_to_response("welcome.html", cont, RequestContext(request))

@login_required
def note_pos(request):
   n = get_object_or_404(Notiz, pk=request.POST.get("id"), ordner__user=request.user)
   form = NotizPosForm(request.POST, instance=n)
   if form.is_valid():
      form.save()
      return HttpResponse("true", mimetype="application/json")
   else:
      raise PermissionDenied()

@login_required
def post_note(request):
   form = NotizForm(request.POST)
   if form.is_valid():
      i = form.save(commit=False)
      if i.ordner.user != request.user:
         raise PermissionDenied()
      else:
         i.save()
         return render_to_response("proxy/mymedia_note.html", { "note": i }, RequestContext(request))
   else:
      raise PermissionDenied()

@login_required
def delete_note(request):
   n = get_object_or_404(Notiz, pk=request.POST.get("id"), ordner__user=request.user)
   n.delete()
   return HttpResponse("true", mimetype="application/json");
      

class MyMediaView(ListView):
   template_name = "proxy/mymedia.html"
   context_object_name = "result"

   def get_context_data(self, **kwargs):
      context = super(MyMediaView, self).get_context_data(**kwargs)
      context["ordner"] = Ordner.objects.get_root_for(self.request.user)
      return context

   def get_queryset(self):
      return Ordner.objects.get_root_for(self.request.user).favorit_set.order_by("order","-added") 

   @method_decorator(login_required)
   def dispatch(self, *args, **kwargs):
      return super(MyMediaView, self).dispatch(*args, **kwargs)

@login_required
def add_provider_ajax(request):
   res = False
   pid = request.POST.get("id")
   if pid:
      try:
         a = Anbieter.objects.get(pk=pid)
      except Anbieter.DoesNotExist:
         pass
      else:
         aset = AnbieterListe.objects.filter(user=request.user).order_by("-order")
         if aset:
            order = aset[0].order + 1
         else:
            order = 0
         try:
            AnbieterListe.objects.create(user=request.user, order=order, anbieter=a)
         except IntegrityError:
            pass
         else:
            res = True
         
         
   return HttpResponse("true" if res else "false", mimetype="application/json")

@login_required
def hide_provider_ajax(request):
   res = False
   pid = request.POST.get("id")
   if pid:
      entries = AnbieterListe.objects.filter(user=request.user, anbieter__id=pid)
      if entries:
         entries.delete()
         res = True
   return HttpResponse("true" if res else "false", mimetype="application/json")


def systematik_ajax(request, sys_nr=None):
   if sys_nr:
      sys = get_object_or_404(Systematik, nr=sys_nr)
   else:
      sys = None

   subs = Systematik.objects.filter(parent=sys)

   resp = {
      "subfolders": [ {"link": reverse(systematik_ajax, args=[f.nr]), "id": f.nr,
         "name": unicode(f) } for f in subs ],
   }
   if sys:
      resp["link"] = reverse(systematik_ajax, args=[sys.nr])
      resp["name"] = unicode(sys)
      resp["id"] = sys.nr
      resp["path"] = [ { "id": x.nr } for x in sys.get_ancestors() ]
   else:
      resp["link"] = reverse(systematik_ajax)

   return HttpResponse(json.dumps(resp), mimetype="application/json")

   

@login_required
def mymedia_ajax(request, folder_id=None):
   """Return the user's favorite (in the specified folder) as rendered <li> items."""

   if folder_id:
      # get the selected folder
      folder = get_object_or_404(Ordner, pk=folder_id, user=request.user)
   else:
      # get the root folder
      folder = Ordner.objects.get_root_for(request.user)

   result = Favorit.objects.filter(user=request.user,ordner=folder).order_by("order", "-added")
   resp = {
      "html": render_to_string("proxy/mymedia_ajax.html", { "ordner": folder, "result": result }, RequestContext(request)),
      "count": folder.get_item_count(),
      "name": folder.name,
      "id": folder.id,
      "link": reverse(mymedia_ajax, args=[folder.id]),
      "path": [{"link": reverse(mymedia_ajax, args=[f.id]), "id": f.id, 
         "name": f.name, "count": f.get_item_count()} for f in folder.get_ancestors()],
      "subfolders": [{"link": reverse(mymedia_ajax, args=[f.id]), "id": f.id, 
         "name": f.name, "count": f.get_item_count()} for f in folder.get_children()],
   }
   return HttpResponse(json.dumps(resp), mimetype="application/json")

@login_required
def rename_folder(request):
   folder_id = request.POST.get("folder_id")
   newname = request.POST.get("name")

   if not folder_id or not newname:
      raise PermissionDenied()

   f = get_object_or_404(Ordner, pk=folder_id, user=request.user)
   
   if f == Ordner.objects.get_root_for(request.user):
      # root folder cannot be renamed
      raise PermissionDenied()

   f.name = newname
   try:
      f.save()
   except IntegrityError:
      # a folder with that name already exists
      raise PermissionDenied()

   res = { 
      "folder_id": f.id, 
      "name": f.name,
      "link": reverse(mymedia_ajax, args=[f.id]),
   }
   return HttpResponse(json.dumps(res), mimetype="application/json")

@login_required
def delete_folder(request):
   folder_id = request.POST.get("folder_id")
   if not folder_id:
      raise PermissionDenied()

   f = get_object_or_404(Ordner, pk=folder_id, user=request.user)

   if f.is_root_node():
      raise PermissionDenied() # root node cannot be deleted

   f.delete()
   return HttpResponse("true", mimetype="application/json")

@login_required
def create_folder(request):
   name = request.POST.get("name")
   parent_id = request.POST.get("parent_id")

   if not name:
      raise PermissionDenied()

   if parent_id:
      parent = get_object_or_404(Ordner, pk=parent_id, user=request.user)
   else:
      parent = Ordner.objects.get_root_for(request.user)

   try:
      f = Ordner.objects.create(name=name, user=request.user, parent=parent)
   except IntegrityError:
      raise PermissionDenied()
   else:
      prev = f.get_previous_sibling()
      prev_id = prev.id if prev else None

      res = {
         "name": f.name,
         "id": f.id,
         "count": f.get_item_count(),
         "link": reverse(mymedia_ajax, args=[f.id]),
         "order": list(parent.get_children().values_list("id", flat=True)),
         "prev_id": prev_id,
      }
      return HttpResponse(json.dumps(res), mimetype="application/json")

def search_autocomplete(request):
   term = request.GET.get("term")
   if term:
      sqs = SearchQuerySet().autocomplete(title_auto=term)
      res = list(set([ result.title for result in sqs ]))[:5]
   else:
      res = []
   return HttpResponse(json.dumps(res))

def download_status(request):
   STATE_DISPLAY = {
      "PENDING": "Vorgemerkt",
      "DOWNLOADING": "Herunterladen",
      "SUCCESS": "Fertig",
      "FAILURE": "Fehlgeschlagen",
   }

   res = []
   if "merlin2lan.cloud" in settings.INSTALLED_APPS:
      org = Organisation.find_by_user(request.user)
      model = CloudDownload
   else:
      org = None
      model = Download

   for mid in request.GET.getlist("medium_id[]"):
      try:
         if model == Download:
            d = Download.objects.get(medium__id=mid)
         else:
            d = CloudDownload.objects.get(medium__id=mid, organisation=org)
      except model.DoesNotExist:
         try:
            m = Medium.objects.get(pk=mid)
         except Medium.DoesNotExist:
            res.append({"id": mid, "status": None})
         else:
            res.append({"id": mid, "status": None, "downloaded": bool(m.download)})
      else:
         ares = d.async_result()
         state = ares.state

         if state == "SUCCESS" and model == Download:
            d.delete()

         if state == "DOWNLOADING":
            meta = ares.result
         else:
            meta = None

         if meta and isinstance(meta, dict):
            count = meta.get("count", 0)
            total = meta.get("total", 0)

            if total > 0:
               percent = round(float(count) / float(total) * 100.0)
            else:
               percent = 0
         else:
            total, count, percent = (0, 0, 0)
            if state == "SUCCESS":
               percent = 100

         data = {
            "id": mid,
            "status_display": STATE_DISPLAY.get(state),
            "status": state,
            "percent": percent, 
            "count": count,
            "total": total,
         }
         res.append(data)

   return HttpResponse(json.dumps(res), mimetype="application/json")

@login_required
def bookmark_detail(request, mid):
   m = get_object_or_404(Medium, pk=mid)
   abgelegt_in = Ordner.objects.filter(favorit__medium=m)

   cont = {
      "medium": m,
      "abgelegt_in": abgelegt_in,
      "tree": Ordner.objects.get_root_for(request.user).get_descendants(include_self=True)
   }

   return render_to_response("proxy/bookmark_detail.html", cont, RequestContext(request))

@login_required
def sort_bookmarks(request):
   ids = request.POST.getlist("item[]")
   sort = {}

   order = 0
   for i in ids:
      sort[i] = order
      order += 1

   for f in Favorit.objects.filter(medium__id__in=ids, user=request.user):
      f.order = sort[str(f.medium.id)]
      f.save()

   return HttpResponse("true", mimetype="application/json")

@login_required
def sort_folders(request):
   ids = request.POST.getlist("item[]")
   neighbor = None

   for i in ids:
      try:
         f = Ordner.objects.get(pk=i)
      except Ordner.DoesNotExist:
         pass
      else:
         if neighbor:
            f.move_to(neighbor, position="right")
         else:
            f.move_to(f.parent, position="first-child")
         neighbor = f

   return HttpResponse("true", mimetype="application/json")

@login_required
def del_bookmark(request, fid=None):
   if fid == None:
      fid = request.POST.get("bookmark_id")
   f = get_object_or_404(Favorit, pk=fid, user=request.user)
   f.delete()
   return HttpResponse("true", mimetype="application/json")

@login_required
def download(request, mid):
   m = get_object_or_404(Medium, pk=mid)
   r = {
      "id": m.id,
   }
   r["poll_status"] = start_download(m, request.user)
   return HttpResponse(json.dumps(r), mimetype="appication/json")

@login_required
def bookmark(request, mid, fid=None):
   m = get_object_or_404(Medium, pk=mid)

   if fid != None:
      folder = get_object_or_404(Ordner, pk=fid, user=request.user)
   else:
      folder = Ordner.objects.get_root_for(request.user)

   f, created = Favorit.objects.get_or_create(user=request.user,medium=m, ordner=folder)

   if not created:
      raise PermissionDenied() # the bookmark exists, indicate that with a 403 error

   r = {
      "id": m.id,
      "folder_id": folder.id,
      "folder_count": folder.favorit_set.all().count(),
   }
   
   if start_download(m, request.user):
      r["poll_status"] = True

   return HttpResponse(json.dumps(r), mimetype="application/json")

def start_download(m, user):
   # schedule the download if not already downloaded
   if m.downloadable:
      if "merlin2lan.cloud" in settings.INSTALLED_APPS:
         o = Organisation.find_by_user(user)
         if o and not o.downloaded(m):
            o.schedule_download(m)
            return True
         
         
      else:
         if not m.download:
            m.schedule_download()
            return True

   

def advanced_search(request):
   cont = {
      "form": SearchForm(),
   }
   return render_to_response("proxy/advanced_search.html", cont, RequestContext(request))

def subitems(request, mid):
   """Return a html rendered list with sub items of an object.""" 
   m = get_object_or_404(Medium, pk=mid)
   items = m.anbieter.get_backend().fetch_by_id_local(m.id_local)[1:]

   cont = {
      "student_view": request.GET.get("student_view"),
      "items": items,
      "subitem_display": True,
      "full_qs": request.GET.get("last_search"),
   }

   return render_to_response("proxy/subitems.html", cont, RequestContext(request))

class SearchView(TemplateView):
   template_name = "proxy/search.html"

   def get_context_data(self, **kwargs):
      context = super(SearchView, self).get_context_data(**kwargs)
      context["stichwort"] = self.request.GET.get("stichwort", "")
      context["anbieter"] = Anbieter.objects.filter(show=True, invisible=False).order_by("order")
      context["systematik"] = Systematik.objects.filter(parent=None)
      if self.request.user.is_authenticated():
         additional = [x.anbieter for x in AnbieterListe.objects.filter(user=self.request.user, anbieter__invisible=False)]
         
         context["anbieter_rest"] = Anbieter.objects.filter(show=False, invisible=False).exclude(anbieterliste__user=self.request.user)
         context["anbieter"] = list(context["anbieter"]) + additional + list(context["anbieter_rest"])

      return context

def search_json(request, anbieter_id=None):
   data = {}
   form = SearchForm(request.GET)
   if form.is_valid():
      rawdata = form.cleaned_data.copy()
   else:
      rawdata = {}

   if not "anbieter" in rawdata or not rawdata["anbieter"]:
      # no provider choosen, try to take the first one
      aset = Anbieter.objects.filter(show=True, invisible=False).order_by("order")
      if len(aset) < 1:
         return HttpResponse(json.dumps({"error": "Keine Medien-Anbieter hinzugefügt."}), mimetype="application/json")
      anbieter = aset[0]
   else:
      anbieter = rawdata["anbieter"]

   try:
      r = anbieter.get_backend().query(rawdata)
   except ElementTree.ParseError as e:
      return HttpResponse(json.dumps({"error": "XML-Fehler: "+unicode(e)}), mimetype="application/json")
   except urllib2.URLError as e:
      return HttpResponse(json.dumps({"error": "HTTP-Fehler: "+unicode(e)}), mimetype="application/json")
   else:

      # reconstruct querystring for further filtering
      qdata = {}
      for key in ("narrow_systematik", "lernressourcentyp", "fachsach", "bildungsebene", "stichwort", "titel", "mediennummer"):
         if key in rawdata and rawdata[key]:
            qdata[key] = rawdata[key]
      qdata["anbieter"] = anbieter.id
      qs = urlencode(qdata, True)
      full_qs = urlencode(request.GET, True)


      if r.total > r.end:
         # Querystring zur naechsten Seite generieren
         next_dict = qdata.copy()
         next_dict["start"] = r.end
         next_qs = urlencode(next_dict, True)
      else:
         next_qs = None

      if r.start > 0:
         # Querystring zur vorherigen Seite generieren
         prev_dict = qdata.copy()
         prev_dict["start"] = max(r.start - r.count, 0)
         prev_qs = urlencode(prev_dict, True)
      else:
         prev_qs = None

      stichwort = rawdata.get("stichwort")

      jsdata = {
         "count": r.total,
         "start": r.start,
         "end": r.end,
         "next_qs": next_qs,
         "prev_qs": prev_qs,
         "stichwort": stichwort, 
         "anbieter_id": anbieter.id,
      }
      if r.count:
         jsdata.update({
            "cur_page": r.start / r.count,
            "last_page": r.total / r.count - 1 if r.total % r.count == 0 else r.total / r.count,
         })

      facets = []
      if r.facets and "systematik" in r.facets:
         for f in r.facets["systematik"]:
            count = f[1]
            if count:
               s = Systematik.objects.get(nr=f[0])
               facets.append((s.id, s.nr, count, unicode(s)))

      jsdata["facets"] = facets
      jsdata["html"] = render_to_string("proxy/resultlist.html", {
         "result": r.items, "qs": qs, "full_qs": full_qs}, RequestContext(request))   

      return HttpResponse(json.dumps(jsdata), mimetype="application/json")

@login_required
def play_page(request, mid, provider_error=False):
   m = get_object_or_404(Medium, pk=mid)
   cont = {
      "medium": m,
      "provider_error": provider_error,
      "last_search": request.GET.get("last_search"),
   }
   return render_to_response("proxy/play_page.html", cont, RequestContext(request))


@login_required
def play(request, mid):
   m = get_object_or_404(Medium, pk=mid)
   if not request.user.has_perm("play_medium", obj=m):
      raise PermissionDenied()

   # check if the request comes from the local network
   if "merlin2lan.cloud" in settings.INSTALLED_APPS:
      is_local = request.session.get("location") == "lan"
      org = Organisation.find_by_user(request.user)
      if org:
         try:
            d = CloudDownload.objects.get(medium=m,organisation=org)
         except CloudDownload.DoesNotExist:
            download = None
         else:
            download = None if not d.path else d
      else:
         download = None
       
   else:
      is_local = is_local_request(request)
      download = m.download

   if download and is_local:
      return HttpResponseRedirect(download.url)
   elif m.protected:
      try:
         downurl = m.anbieter.get_backend().get_protected_url(m)
      except urllib2.URLError:
         return play_page(request, m.id, provider_error=True)

      if downurl:
         return HttpResponseRedirect(downurl)
      else: 
         raise PermissionDenied()
   elif m.media_url:
      return HttpResponseRedirect(m.media_url)
   else:
      raise PermissionDenied()
