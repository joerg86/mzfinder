from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

class Command(BaseCommand):
    args = '<group_name> <email_domain> <group_file>'
    help = 'Extract users from a specified group in an /etc/group-like file.'

    def handle(self, *args, **options):
       f = open(args[2], "r")
       domain = args[1]
       group = args[0]
       for line in f:
          if line.startswith(group+":"):
             for username in line.split(":")[3].split(","):
	         username = username.strip()
                 u, c = User.objects.get_or_create(username=username, defaults={"is_active": False, "email": username + "@" + domain})
                 if c:
                     u.set_unusable_password()
                     u.save()
                     self.stdout.write("User %s was created.\n" % username)

