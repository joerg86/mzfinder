from django.core.management.base import BaseCommand, CommandError
from mzkatalog.models import Katalog, Medium, Anbieter, Systematik
from mzkatalog.eaf import EAFFile
from django.utils.html import strip_tags
import urllib2

class Command(BaseCommand):
    help = "Importiert FWU-Systematik via FTP"

    def add_arguments(self, parser):
        parser.add_argument('--clear', action="store_true")

    def handle(self, *args, **options):
        if options["clear"]:
            Systematik.objects.all().delete()

        for line in urllib2.urlopen("ftp://ftp.fwu.de/fwu/eaf/eafsys.txt"):
            line = line.decode("latin1")
            sysNr, text = line.split("#")
            sysNr = sysNr.strip()
            text = text.strip()
            o, created = Systematik.objects.get_or_create(nr=sysNr, defaults={ "text" : text })

        for s in Systematik.objects.all():
            l = len(s.nr)
            for x in range(1, l-1):
                cut = s.nr[:-x]
                try:
                    p = Systematik.objects.get(nr=cut)
                except Systematik.DoesNotExist:
                    pass
                else:
                    if s.parent != p:
                        s.parent = p
                        s.save()
                    break



