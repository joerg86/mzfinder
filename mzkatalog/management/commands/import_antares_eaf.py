from django.core.management.base import BaseCommand, CommandError
from mzkatalog.models import Katalog, Medium, Anbieter, Systematik, Medienart
from mzkatalog.eaf import EAFFile
from django.utils.html import strip_tags

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('anbieter_id', type=int)
        parser.add_argument('eaf_file', type=str)
        parser.add_argument('--clear', action="store_true")
        parser.add_argument('--context')
        parser.add_argument('--src', default="offline")

    def handle(self, *args, **options):
        e = EAFFile(options["eaf_file" ])
        anbieter = Anbieter.objects.get(pk=options["anbieter_id"])

        context = options["context"]
        land, kreis = context.split("/", 1)
        prefix = kreis.upper()

        if options["clear"]:
            anbieter.medium_set.all().delete()

        existing = anbieter.medium_set.values_list("id_local", flat=True)

        # Felder fuer Suchdokument
        eaf_fields = ["A10", "A50", "D10", "D14", "D16", "E10", "E12", "E14", "E16", "F10", "H40", "H50", "I10", "I20", "I30", "I40", "I50", "I60", "I70", "I75", "I80", "M60", "M65", "M70", "M80" ] 
            
        count = 0
        bulk_insert = []
        already_added = set()


        # dictionary fuer schnellen zugriff auf systematiken
        sysdict = dict([(s.nr, s) for s in Systematik.objects.all()])
        martdict = dict([(ma.nr, ma) for ma in Medienart.objects.all()])

        for item in e.items():
            id_local = item["A10"]
            if "A11" in item: # Verleihkatalog
                sig = item["A11"]
                sig = sig.replace(" ", "") # Leerzeichen entfernen
                id_local = prefix + "-" + sig
            elif id_local.startswith("-"):
                id_local = prefix + id_local

            if not id_local in existing and not id_local in already_added:
                doc = []
                for f in eaf_fields:
                   if f in item:
                        doc.append(item[f])

                """  
                for snr in i["systematik"]:
                """

                # id_local normalisieren
                id_loc_norm = id_local
                if "-" in id_local:
                    dokst, mnr = id_local.split("-", 1)
                    try:
                        mnr = int(mnr)
                    except ValueError:
                        pass
                    else:
                        id_loc_norm = "%s-%d" % (dokst, mnr)

                defaults = {
                    "id_local": id_local,
                    "anbieter": anbieter,
                    "title": strip_tags(item.get("E10", "")),
                    "description": item.get("I40"),
                    "thumbnail": "https://cover.datenbank-bildungsmedien.net/100x75/%s.png" % id_loc_norm,
                }
                
                if "M10" in item and item["M10"].strip() in martdict:
                    defaults["medienart"] = martdict[item["M10"].strip()]


                defaults["datasheet"] = "http://%s-%s.datenbank-bildungsmedien.net/record?src=%s&id=%s" % (land.lower(), kreis.lower(), options["src"], id_loc_norm)
                defaults["media_url"] = "http://%s-%s.datenbank-bildungsmedien.net/record?src=%s&id=%s" % (land.lower(), kreis.lower(), options["src"], id_loc_norm)

                if doc:
                    doc = u"\n".join(doc)
                    defaults["search_additional"] = doc

                m = Medium(**defaults)
                m.save()

                if "G10" in item:
                    for snr in item["G10"].split(" "):
                        snr = snr.strip()
                        if snr in sysdict:
                            m.systematik.add(sysdict[snr])

                already_added.add(id_local)
                count += 1

        self.stdout.write('%d Medien importiert.' % count)
