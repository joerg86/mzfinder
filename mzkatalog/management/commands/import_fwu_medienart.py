from django.core.management.base import BaseCommand, CommandError
from mzkatalog.models import Katalog, Medium, Anbieter, Medienart
from mzkatalog.eaf import EAFFile
from django.utils.html import strip_tags
import urllib2

class Command(BaseCommand):
    help = "Importiert FWU-Medienarten via FTP"

    def add_arguments(self, parser):
        parser.add_argument('--clear', action="store_true")

    def handle(self, *args, **options):
        if options["clear"]:
            Medienart.objects.all().delete()

        for line in urllib2.urlopen("ftp://ftp.fwu.de/fwu/eaf/eafmed.txt"):
            line = line.decode("latin1")
            medNr, text = line.split("#")
            medNr = medNr.strip()
            text = text.strip()
            o, created = Medienart.objects.get_or_create(nr=medNr, defaults={ "text" : text })




