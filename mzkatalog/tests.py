from django.test import TestCase
from django.contrib.auth.models import User
from models import Favorit, Medium, Ordner, Anbieter, Notiz 
from util import is_local_ip
import views
import json
import random
import tempfile
import os.path

XMLFILE = "file://" + os.path.abspath(os.path.join(os.path.dirname(__file__), "fixtures/test_suche.xml#"))

class M2LTestCase(TestCase):
   fixtures = [ "test_anbieter.json" ]

   def setUp(self):
      anbieter = self.anbieter = Anbieter.objects.get(pk=1)
      anbieter.url = XMLFILE
      anbieter.save()

class BackendTest(M2LTestCase):
   def test_broken_nibis(self):
      """Tests if errors are handled correctly if the backend is broken."""
      anbieter = self.anbieter
      urls = [ "http://localhost:112234/asdfasdf", "http://search.merlin.nibis.de/suche.php", "http://google.de" ]
      for url in urls:
         anbieter.url = url
         anbieter.save()
         resp = self.client.get("/proxy/search.json", {"stichwort": "halloween"})
         data = json.loads(resp.content)
         self.assertTrue("error" in data)

   def test_valid_result(self):
      """Test if a valid XML fixture is correctly read."""
      resp = self.client.get("/proxy/search.json")

      data = json.loads(resp.content)
      self.assertEqual(data["count"], 2)
      self.assertEqual(data["start"], 0)
      self.assertEqual(data["last_page"], 0)
      self.assertEqual(len(Medium.objects.all()), 2)

class MiscTest(M2LTestCase):
   fixtures = [ "test_anbieter.json", "test_medien.json" ]

   def test_is_local_ip(self):
      data = [
         ("213.111.133.123", False),
         ("127.0.0.1", True),
         ("172.16.200.1", True),
         ("10.1.1.1", True),
         ("1.2.3.4", False),
         ("192.168.1.13", True),
      ]
      for ip, expect in data:
         self.assertEquals(expect, is_local_ip(ip))

   def test_search_autocomplete_empty(self):
      resp = self.client.get("/proxy/ajax/search_autocomplete", {"term": "6asdf0er"})
      data=json.loads(resp.content)
      self.assertEquals(len(data), 0)
   
   def test_search_autocomplete(self):
      resp = self.client.get("/proxy/ajax/search_autocomplete", {"term": "Sitcom"})
      data=json.loads(resp.content)
      self.assertEquals(len(data), 1)

   def test_subitems(self):
      m = Medium.objects.order_by("?")[0]
      resp = self.client.get("/proxy/subitems/%s" % m.id)
      items = resp.context["items"]
      self.assertEquals(len(items), 1)


class DownloadTest(M2LTestCase):
   fixtures = [ "test_anbieter.json", "test_medien.json" ]

   def setUp(self):
      super(DownloadTest, self).setUp()
      self.fred = User.objects.create_user("fred", "fred@fred.com", "fred")
      self.client.login(username="fred", password="fred")

   def test_download_status_finish(self):
      """Test if a finished download returns the correct state."""
      m = Medium.objects.get(pk=1)
      m.download = "cache/foo.wmv"
      m.save()
      resp = self.client.get("/proxy/ajax/download_status", { "medium_id[]" : [m.id]})
      data = json.loads(resp.content)
      self.assertEquals(data[0]["status"], None)

   def test_download_redirection(self):
      """Test if we get redirected to a particular URL if it's not a merlin."""
      m = Medium.objects.get(pk=1)
      m.protected = False
      m.download = None
      m.media_url = "http://www.google.com"
      m.save()
      resp = self.client.get("/proxy/play/%s" % m.id)
      self.assertRedirects(resp, "http://www.google.com")

   def test_download_fail(self):
      """Test if a download that doesn't exist give us 404."""
      resp = self.client.get("/proxy/play/1233333333")
      self.assertEquals(resp.status_code, 404)

class MyMediaTest(M2LTestCase):
   fixtures = [ "test_anbieter.json", "test_medien.json" ]

   def setUp(self):
      super(MyMediaTest, self).setUp()
      self.fred = User.objects.create_user("fred", "fred@fred.com", "fred")
      self.client.login(username="fred", password="fred")

   def _make_random_bookmarks(self, count):
      ids = []
      order = 0
      folder = Ordner.objects.get_root_for(self.fred)
      for m in Medium.objects.all().order_by("?")[:count]:
         f = Favorit.objects.create(user=self.fred, medium=m, order=order, ordner=folder)
         ids.append(f.id)
         order += 1
      return ids

   def _make_random_folders(self, count):
      ids = []
      for i in xrange(count):
         root = Ordner.objects.get_root_for(self.fred)
         f = Ordner.objects.create(user=self.fred,name="folder%d" % i,parent=root)
         ids.append(f.id)
      return ids

   def test_delete_note(self):
      """Test note deletion"""
      moni = User.objects.create_user("moni", "moni@moni.com", "moni")
      root = Ordner.objects.get_root_for(moni)
      n = Notiz.objects.create(ordner=root, text="bla bla bla", css_class="blue")
      
      r = self.client.post("/proxy/ajax/delete_note", {"id": n.id })
      self.assertEquals(r.status_code, 404)
      self.assertEquals(Notiz.objects.filter(pk=n.id).count(), 1)

      self.client.login(username="moni", password="moni")
      self.client.post("/proxy/ajax/delete_note", {"id": n.id })
      self.assertEquals(Notiz.objects.filter(pk=n.id).count(), 0)

   def test_note_pos(self):
      moni = User.objects.create_user("moni", "moni@moni.com", "moni")
      root = Ordner.objects.get_root_for(self.fred)
      n = Notiz.objects.create(ordner=root, text="bla bla bla", css_class="blue")
      for i in range(10):
         pos_x = random.randint(0, 1000)
         pos_y = random.randint(0, 1000)

         self.client.login(username="fred", password="fred")
         r = self.client.post("/proxy/ajax/note_pos", { "id": n.id, "pos_x": pos_x, "pos_y": pos_y })
         self.assertEquals(r.status_code, 200)
         n = Notiz.objects.get(pk=n.id)
         self.assertEquals(n.pos_x, pos_x)
         self.assertEquals(n.pos_y, pos_y)

         self.client.login(username="moni", password="moni")
         r = self.client.post("/proxy/ajax/note_pos", { "id": n.id, "pos_x": pos_x, "pos_y": pos_y })
         self.assertEquals(r.status_code, 404)


   def test_post_note(self):
      """Test if we can post a note to a folder."""
      moni = User.objects.create_user("moni", "moni@moni.com", "moni")
      root = Ordner.objects.get_root_for(self.fred)
      fids = [root.id] + self._make_random_folders(10)
      text = "Hello World"
      for fid in fids:
         self.client.login(username="fred", password="fred")
         r = self.client.post("/proxy/ajax/post_note", { "text": text, "ordner": fid, "css_class": "yellow"})
         self.assertEquals(r.status_code, 200)
         n = Notiz.objects.get(text=text,css_class="yellow", ordner__id=fid)
         self.assertEquals(r.context["note"], n)

         self.client.login(username="moni", password="moni")
         r = self.client.post("/proxy/ajax/post_note", { "text": text, "ordner": fid, "css_class": "yellow"})
         self.assertEquals(r.status_code, 403)

      

   def test_bookmark(self):
      """Test if we can bookmark an item."""
      items = Medium.objects.all().order_by("?")[:10]
      item_ids = sorted(list(items.values_list("id", flat=True)))
      Medium.objects.filter(pk__in=item_ids).update(protected=False) # prevent celery from downloading
      for itemid in item_ids:
         self.client.get("/proxy/ajax/bookmark/%d" % itemid)
      folder = Ordner.objects.get_root_for(self.fred)
      bookmark_ids = sorted(list(Medium.objects.filter(favorit__ordner=folder).values_list("id", flat=True)))
      self.assertEquals(bookmark_ids, item_ids)

   def test_del_bookmark(self):
      """Test bookmark deletion."""
      ids = self._make_random_bookmarks(5)
      remaining = 5
      for fid in ids:
         self.client.get("/proxy/ajax/del_bookmark/%d" % fid)
         remaining -= 1
         self.assertEquals(len(Favorit.objects.all()), remaining)

   def test_rename_root_folder(self):
      root = Ordner.objects.get_root_for(self.fred)
      resp = self.client.post("/proxy/ajax/rename-folder", { "folder_id": root.id, "name": "test" })
      self.assertEquals(resp.status_code, 403)

   def test_rename_folder(self):
      """Test if we can rename a folder."""
      ids = self._make_random_folders(5)
      for folder_id in ids:
         f = Ordner.objects.get(pk=folder_id)
         newname = f.name + "_"
         resp = self.client.post("/proxy/ajax/rename-folder", { "folder_id": folder_id, "name": newname })
         data = json.loads(resp.content)
         
         # reload the object from the DB
         f = Ordner.objects.get(pk=folder_id)

         self.assertEquals(newname, data["name"])
         self.assertEquals(f.name, newname)

   def test_rename_folder_fail(self):
      """Test if creating a folder with an existing name fails."""
      root = Ordner.objects.get_root_for(self.fred)
      f1 = Ordner.objects.create(name="test", user=self.fred, parent=root)
      f2 = Ordner.objects.create(name="test2", user=self.fred, parent=root)
      resp = self.client.post("/proxy/ajax/rename-folder", { "folder_id": f2.id, "name": "test" })
      self.assertEquals(resp.status_code, 403)
      

   def test_delete_folder(self):
      """Test if we can delete a folder."""
      ids = self._make_random_folders(5)
      for folder_id in ids:
         resp = self.client.post("/proxy/ajax/delete-folder", { "folder_id": folder_id })
         self.assertEquals("true", resp.content) # should return json true on success
         self.assertEquals(Ordner.objects.filter(pk=folder_id).count(), 0)

   def test_delete_root_folder(self):
      """Test if we cannot delete the root folder."""
      f = Ordner.objects.get_root_for(self.fred)
      resp = self.client.post("/proxy/ajax/delete-folder", { "folder_id": f.id })
      self.assertEquals(resp.status_code, 403)

   def test_create_folder_flat(self):
      """ Test if folder creation works (flat). """
      for i in xrange(10):
         resp = self.client.post("/proxy/ajax/create-folder", { "name": "test%d" % i })
         data = json.loads(resp.content)
         f = Ordner.objects.get(pk=data["id"])
         self.assertEquals(f.name, "test%d" % i)
         self.assertEquals(0, data["count"])
         self.assertEquals(len(f.get_siblings(include_self=True)), len(data["order"]))

   def test_create_folder_tree(self):
      """ Test if folder creation works (tree). """
      parent = Ordner.objects.get_root_for(self.fred).id
      for i in xrange(19):
         resp = self.client.post("/proxy/ajax/create-folder", { "name": "test%d" % i, "parent_id": parent })
         data = json.loads(resp.content)
         f = Ordner.objects.get(pk=data["id"])
         self.assertEquals(f.name, "test%d" % i)
         self.assertEquals(0, data["count"])
         self.assertEquals(len(f.get_siblings(include_self=True)), 1)
         self.assertEquals(f.parent.id, parent)
         parent = f.id

   def test_sort_bookmarks(self):
      """Test if sorting of bookmarks works."""
      ids = list(Medium.objects.filter(favorit__id__in=self._make_random_bookmarks(5)).values_list("id", flat=True))
      random.seed()
      for i in xrange(10):
         random.shuffle(ids)
         resp = self.client.post("/proxy/ajax/sort-bookmarks", { "item[]": ids })
         neworder = list(Medium.objects.filter(favorit__user=self.fred).order_by("favorit__order", "-favorit__added").values_list("id", flat=True))
         self.assertEquals(ids, neworder)

   def test_sort_folders(self):
      """Test if sorting of folders works."""
      ids = self._make_random_folders(5)
      random.seed()
      for i in xrange(10):
         random.shuffle(ids)
         resp = self.client.post("/proxy/ajax/sort-folders", { "item[]": ids })
         neworder = list(Ordner.objects.get_root_for(self.fred).get_children().values_list("id", flat=True))
         self.assertEquals(ids, neworder)

   def test_duplicate_folder(self):
      """Test if creation of a folder that already exists fails."""
      resp = self.client.post("/proxy/ajax/create-folder", { "name": "test1" })
      resp = self.client.post("/proxy/ajax/create-folder", { "name": "test1" })
      self.assertEquals(resp.status_code, 403)

   def test_mymedia_list_empty(self):
      """Test the non-ajax mymedia view without bookmarks."""
      resp = self.client.get("/proxy/mymedia")
      self.assertEquals(len(resp.context["result"]), 0)

   def test_mymedia_list(self):
      """Test the non-ajax mymedia view with five bookmarks."""
      ids = self._make_random_bookmarks(6)
      extra = ids.pop() # extra bookmark that we move away from the main folder
      folder_id = self._make_random_folders(1)[0]
      Favorit.objects.filter(pk=extra).update(ordner=folder_id)

      resp = self.client.get("/proxy/mymedia")
      idx = 0
      self.assertEquals(len(resp.context["ordner"].get_children()), 1)
      self.assertEquals(len(resp.context["result"]), 5)
      for f in resp.context["result"]:
         self.assertEquals(f.id, ids[idx])
         idx += 1

   def test_mymedia_list_folders(self):
      """Test if created folders are listed."""
      ids = self._make_random_folders(5)
      resp = self.client.get("/proxy/mymedia")
      for f in resp.context["ordner"].get_children():
         self.assertTrue(f.id in ids)

   def test_mymedia_list_ajax_empty(self):
      """Test without an bookmarks."""
      resp = self.client.get("/proxy/ajax/mymedia")
      data = json.loads(resp.content)
      self.assertEquals(data["count"], 0) 

   def test_mymedia_list_ajax(self):
      """Test with five bookmarks."""
      bids = self._make_random_bookmarks(6)
      fids = self._make_random_folders(5)
      root = Ordner.objects.get_root_for(self.fred).id
      
      for f in Favorit.objects.filter(pk__in=bids):
         try:
            f.ordner_id = fids.pop()
         except IndexError:
            break
         else:
            f.save()

      for f in [None, root] + fids:
         if f == None:
            resp = self.client.get("/proxy/ajax/mymedia")
         else:
            resp = self.client.get("/proxy/ajax/mymedia/%d"% f)
         data = json.loads(resp.content)

         if f == None or f == root:
            self.assertEquals(data["count"], 6) # 1 Favorit + 5 Ordner 
         else:
            self.assertEquals(data["count"], 1) 

      
