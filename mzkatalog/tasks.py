from celery.task import task
import tempfile
import urllib2
import re
import os.path
from django.core.files import File
from datetime import datetime, timedelta

CDIS_RE = re.compile(r".*; filename=\"(.*)\"")

@task
def download(medium):
   if medium.protected:
      downurl = medium.anbieter.get_backend().get_protected_url(medium)
   else:
      downurl = medium.media_url 

   t = tempfile.NamedTemporaryFile()
   r = urllib2.urlopen(downurl)

   filename = os.path.basename(downurl)
   cdis = r.headers.get("content-disposition","")
   match = CDIS_RE.match(cdis)
   if match:
      filename = match.group(1)
  
   length = r.headers.get("content-length")
   try:
      length = int(length)
   except ValueError:
      length = 0

   status = {}   
   status["total"] = length
   status["count"] = 0
   download.update_state(state="WAITING")

   count = 0
 
   stamp = None 
   interval = timedelta(seconds=2) # status update every two seconds
   while True:
      data = r.read(8*1024) 
      if len(data) == 0:
         break
      else:
         count += len(data)
         t.write(data)
         if not stamp or datetime.now() > stamp + interval:
            status["count"] = count
            download.update_state(state="DOWNLOADING", meta=status)            
            stamp = datetime.now()

   medium.download.save(filename, File(t))
   t.close()
   
