#+-+ coding: utf-8 +-+
ALIAS_MAP = {
    
}

class EAFFile(object):
    def items(self): # iterator
        with open(self.fn, "r") as f:
            curitem = None
            for line in f:
                field = line[:3].upper()
                cont = line[3:-2].decode("cp1252")
                if field == "A10": # ID-Feld
                    if curitem:
                        yield curitem
                    curitem = {}
                    #self.items.append(curitem)
                    #self.byid[cont] = curitem
                if curitem != None:
                    d = curitem
                    if field in d:
                        d[field] += " " + cont
                    else:
                        d[field] = cont
            if curitem:
                yield curitem
            
            
    def __init__(self, fn):
        self.fn = fn

