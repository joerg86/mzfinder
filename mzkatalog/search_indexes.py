import datetime
from haystack import indexes
from mzkatalog.models import Medium


class MediumIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    anbieter_id = indexes.IntegerField(model_attr="anbieter_id")
    medienart = indexes.IntegerField(model_attr="medienart_id", null=True, faceted=True)
    systematik = indexes.MultiValueField(faceted=True)
    title = indexes.CharField(null=True,model_attr="title")
    title_auto = indexes.EdgeNgramField(null=True,model_attr="title")

    def prepare_systematik(self, obj):
        r = set()
        for s in obj.systematik.all():
            r.add(s.id)
            [ r.add(x.id) for x in s.get_ancestors() ]
        return list(r)

    def get_model(self):
        return Medium

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return Medium.objects.filter(anbieter__backend__in=("local","merlin-hybrid"), parent__isnull=True)


