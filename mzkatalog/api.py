# +-+ coding: utf-8 +-+
from tastypie.resources import ModelResource
from mzkatalog.models import Medium, Anbieter, Katalog, Systematik, Kunde, Medienart
from django.conf.urls import url
from django.core.paginator import Paginator, InvalidPage
from tastypie.api import Api
from tastypie.utils import trailing_slash
from tastypie import fields
from django.shortcuts import get_object_or_404

class KundeResource(ModelResource):
    class Meta:
        queryset = Kunde.objects.all()
        fields = ["slug", "name", "short", "url" ]
        detail_uri_name = "slug"

class AnbieterResource(ModelResource):
    kunde = fields.CharField("kunde__slug", null=True, blank=True)
    class Meta:
        queryset = Anbieter.objects.all()
        fields = ["id", "name", "backend", "logo", "user_warning"]

        filtering = {
            "kunde": ["exact", "isnull"],
        }

class MedienartResource(ModelResource):
    class Meta:
        queryset = Medienart.objects.all()
        fields = ["id", "text", "nr", "icon"]
        filtering = {
            "id": ["exact", "in"],
        }

class SystematikResource(ModelResource):
    class Meta:
        queryset = Systematik.objects.all()
        fields = ["id", "text", "nr"]
        filtering = {
            "id": ["exact", "in"],
        }


class MediumResource(ModelResource):
    #anbieter = fields.OneToOneField("mzkatalog.api.AnbieterResource", "anbieter")
    systematik = fields.OneToManyField("mzkatalog.api.SystematikResource", "systematik", null=True, blank=True, full=True)
    medienart = fields.OneToOneField("mzkatalog.api.MedienartResource", "medienart", null=True, blank=True, full=True)

    class Meta:
        queryset = Medium.objects.all().select_related("medienart").prefetch_related("systematik")
        fields = [ "datasheet", "description", "id", "media_url", "thumbnail", "title" ]

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_search'), name="api_get_search"),
        ]

    def get_search(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if not request.GET.get("q"):
            raise KeyError("Kein Stichwort gefunden.")

        self.is_authenticated(request)
        self.throttle_check(request)

        # no provider choosen, try to take the first one
        if request.GET.get("anbieter"):
            anbieter = get_object_or_404(Anbieter, pk=request.GET["anbieter"])
        else:
            anbieter = Anbieter.objects.filter(show=True, invisible=False).order_by("order").first()

        offset = int(request.GET["offset"]) if "offset" in request.GET else 0
        limit = int(request.GET["limit"]) if "limit" in request.GET else 20
        
        if not anbieter:
            raise Http404("Keine Medien-Anbieter hinzugefügt.")

        r = anbieter.get_backend().query({ "stichwort": request.GET.get("q"), "medienart": request.GET.getlist("medienart"), "systematik": request.GET.getlist("systematik"), "limit": limit, "offset": offset})

        objects = []

        for result in r.items:
            bundle = self.build_bundle(obj=result, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)
    
        meta = {
            "total": r.total,
            "facets": r.facets,
        }

        object_list = {
            "meta": meta,
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)



v1_api = Api(api_name="v1")
v1_api.register(MediumResource())
v1_api.register(AnbieterResource())
v1_api.register(KundeResource())
v1_api.register(SystematikResource())
v1_api.register(MedienartResource())
