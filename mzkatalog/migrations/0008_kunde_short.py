# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mzkatalog', '0007_auto_20160215_0749'),
    ]

    operations = [
        migrations.AddField(
            model_name='kunde',
            name='short',
            field=models.CharField(default='Migration', max_length=100),
            preserve_default=False,
        ),
    ]
