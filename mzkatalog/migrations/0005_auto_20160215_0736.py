# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('mzkatalog', '0004_kunde_slug'),
    ]

    operations = [
        migrations.RunSQL("update mzkatalog_kunde set slug=id"),    
    ]
