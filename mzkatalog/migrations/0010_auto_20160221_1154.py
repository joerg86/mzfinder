# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mzkatalog', '0009_medienart'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='medienart',
            options={'ordering': ('nr',), 'verbose_name': 'Medienart', 'verbose_name_plural': 'Medienarten'},
        ),
        migrations.AddField(
            model_name='medium',
            name='medienart',
            field=models.ForeignKey(blank=True, to='mzkatalog.Medienart', null=True),
        ),
    ]
