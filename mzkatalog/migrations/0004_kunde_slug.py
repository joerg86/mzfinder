# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mzkatalog', '0003_anbieter_kunde'),
    ]

    operations = [
        migrations.AddField(
            model_name='kunde',
            name='slug',
            field=models.SlugField(null=True, blank=True),
        ),
    ]
