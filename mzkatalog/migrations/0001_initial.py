# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields
import mptt.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Anbieter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('backend', models.CharField(max_length=50, verbose_name=b'Schnittstelle', choices=[(b'arix', b'Antares via ARI/X'), (b'merlin-hybrid', b'Merlin-Hybrid (lokal & NiBis)'), (b'merlin-xml', b'NiBis-Merlin-XML'), (b'local', b'Lokale Datenbank'), (b'youtube', b'YouTube')])),
                ('url', models.URLField(null=True, verbose_name=b'URL', blank=True)),
                ('username', models.CharField(max_length=200, null=True, verbose_name=b'Benutzername', blank=True)),
                ('password', models.CharField(max_length=200, null=True, verbose_name=b'Passwort', blank=True)),
                ('config', jsonfield.fields.JSONField(null=True, verbose_name=b'Erweiterte Einstellungen (JSON)', blank=True)),
                ('order', models.PositiveIntegerField(default=0, verbose_name=b'Reihenfolge')),
                ('logo', models.ImageField(default=None, null=True, upload_to=b'img/provider_logos', blank=True)),
                ('show', models.BooleanField(default=True, help_text=b'Standardm\xc3\xa4\xc3\x9fig bei allen Nutzern anzeigen.', verbose_name=b'Anzeigen')),
                ('invisible', models.BooleanField(default=False, help_text=b'Der Anbieter wird nicht in der Suchmaschine angezeigt oder zum Hinzuf\xc3\xbcgen angeboten', verbose_name=b'Unsichtbar')),
                ('user_warning', models.TextField(help_text=b'Warnmeldung mit Hinweis auf Nutzungsbedingungen, Urheberrecht etc.', null=True, verbose_name=b'Benutzer-Warnung', blank=True)),
            ],
            options={
                'ordering': ('order', 'name'),
                'verbose_name': 'Anbieter',
                'verbose_name_plural': 'Anbieter',
            },
        ),
        migrations.CreateModel(
            name='AnbieterListe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField()),
                ('anbieter', models.ForeignKey(to='mzkatalog.Anbieter')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': 'Anbieter-Liste',
                'verbose_name_plural': 'Anbieter-Listen',
            },
        ),
        migrations.CreateModel(
            name='Download',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('task_id', models.CharField(unique=True, max_length=255)),
                ('started', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Favorit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('added', models.DateField(auto_now_add=True)),
                ('order', models.IntegerField(default=0)),
            ],
            options={
                'verbose_name_plural': 'Favoriten',
            },
        ),
        migrations.CreateModel(
            name='Katalog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('url', models.URLField(null=True, blank=True)),
                ('logo_url', models.URLField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Katalog',
                'verbose_name_plural': 'Kataloge',
            },
        ),
        migrations.CreateModel(
            name='Medium',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('id_local', models.CharField(max_length=255, verbose_name=b'Lokale ID')),
                ('title', models.CharField(max_length=255, null=True, verbose_name=b'Titel', blank=True)),
                ('description', models.TextField(null=True, verbose_name=b'Beschreibung', blank=True)),
                ('datasheet', models.URLField(max_length=255, null=True, blank=True)),
                ('thumbnail', models.URLField(max_length=255, null=True, blank=True)),
                ('animated', models.URLField(max_length=255, null=True, blank=True)),
                ('logo', models.URLField(null=True, blank=True)),
                ('protected', models.BooleanField(default=True)),
                ('subitem_cnt', models.PositiveIntegerField(null=True, blank=True)),
                ('download', models.FileField(null=True, upload_to=b'cache', blank=True)),
                ('media_url', models.URLField(null=True, verbose_name=b'Medien-URL', blank=True)),
                ('path', models.CharField(max_length=255, null=True, blank=True)),
                ('downloadable', models.BooleanField(default=False)),
                ('playable', models.BooleanField(default=True)),
                ('search_additional', models.TextField(null=True, verbose_name=b'Zusatzinfos f\xc3\xbcr Suchmaschine', blank=True)),
                ('anbieter', models.ForeignKey(to='mzkatalog.Anbieter')),
                ('bookmarkers', models.ManyToManyField(to=settings.AUTH_USER_MODEL, through='mzkatalog.Favorit')),
                ('katalog', models.ForeignKey(blank=True, to='mzkatalog.Katalog', null=True)),
                ('owner', models.ForeignKey(related_name='own_media_set', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('parent', models.ForeignKey(blank=True, to='mzkatalog.Medium', null=True)),
            ],
            options={
                'verbose_name': 'Medium',
                'verbose_name_plural': 'Medien',
            },
        ),
        migrations.CreateModel(
            name='Mimetype',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('major', models.CharField(max_length=100)),
                ('minor', models.CharField(max_length=100, null=True, blank=True)),
                ('description', models.CharField(max_length=100)),
                ('icon', models.ImageField(upload_to=b'img/mimetypes/16')),
            ],
        ),
        migrations.CreateModel(
            name='Notiz',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('css_class', models.CharField(max_length=60)),
                ('pos_x', models.PositiveIntegerField(default=0)),
                ('pos_y', models.PositiveIntegerField(default=0)),
            ],
            options={
                'verbose_name': 'Notiz',
                'verbose_name_plural': 'Notizen',
            },
        ),
        migrations.CreateModel(
            name='Ordner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='mzkatalog.Ordner', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'Ordner',
                'verbose_name_plural': 'Ordner',
            },
        ),
        migrations.CreateModel(
            name='Systematik',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nr', models.CharField(unique=True, max_length=11)),
                ('text', models.CharField(max_length=100)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='mzkatalog.Systematik', null=True)),
            ],
            options={
                'verbose_name': 'Systematik',
                'verbose_name_plural': 'Systematik',
            },
        ),
        migrations.AddField(
            model_name='notiz',
            name='ordner',
            field=models.ForeignKey(to='mzkatalog.Ordner'),
        ),
        migrations.AlterUniqueTogether(
            name='mimetype',
            unique_together=set([('major', 'minor')]),
        ),
        migrations.AddField(
            model_name='medium',
            name='systematik',
            field=models.ManyToManyField(to='mzkatalog.Systematik', blank=True),
        ),
        migrations.AddField(
            model_name='favorit',
            name='medium',
            field=models.ForeignKey(to='mzkatalog.Medium'),
        ),
        migrations.AddField(
            model_name='favorit',
            name='ordner',
            field=models.ForeignKey(to='mzkatalog.Ordner'),
        ),
        migrations.AddField(
            model_name='favorit',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='download',
            name='medium',
            field=models.OneToOneField(related_name='downloading', to='mzkatalog.Medium'),
        ),
        migrations.AlterUniqueTogether(
            name='ordner',
            unique_together=set([('user', 'name', 'parent')]),
        ),
        migrations.AlterUniqueTogether(
            name='medium',
            unique_together=set([('id_local', 'anbieter')]),
        ),
        migrations.AlterUniqueTogether(
            name='favorit',
            unique_together=set([('user', 'medium', 'ordner')]),
        ),
        migrations.AlterUniqueTogether(
            name='anbieterliste',
            unique_together=set([('anbieter', 'user')]),
        ),
    ]
