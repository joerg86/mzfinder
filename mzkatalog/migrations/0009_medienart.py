# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mzkatalog', '0008_kunde_short'),
    ]

    operations = [
        migrations.CreateModel(
            name='Medienart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nr', models.CharField(unique=True, max_length=11)),
                ('text', models.CharField(max_length=100)),
                ('icon', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Medienart',
                'verbose_name_plural': 'Medienarten',
            },
        ),
    ]
