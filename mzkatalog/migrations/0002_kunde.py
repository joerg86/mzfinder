# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mzkatalog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Kunde',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('url', models.URLField(null=True, blank=True)),
                ('adresse', models.CharField(max_length=100, null=True, verbose_name=b'Adresse', blank=True)),
                ('adresse2', models.CharField(max_length=100, null=True, verbose_name=b'Adresse (2. Zeile)', blank=True)),
                ('plz', models.CharField(max_length=5, null=True, verbose_name=b'PLZ', blank=True)),
                ('ort', models.CharField(max_length=100, null=True, verbose_name=b'Ort', blank=True)),
                ('admin', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Kunde',
                'verbose_name_plural': 'Kunden',
            },
        ),
    ]
