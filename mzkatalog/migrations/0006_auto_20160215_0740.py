# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mzkatalog', '0005_auto_20160215_0736'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kunde',
            name='slug',
            field=models.SlugField(),
        ),
    ]
