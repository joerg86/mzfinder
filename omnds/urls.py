from django.conf.urls import patterns, url, include
from omnds.views import AppView
from mzkatalog.api import v1_api

app_name = "omnds"
urlpatterns = patterns("",
    url(r"^api/", include(v1_api.urls)),
    url(r"^$", AppView.as_view(), name="app"),
)
