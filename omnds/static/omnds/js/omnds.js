angular.module("omnds", ["ng.django.urls", "ng.django.static", "restangular", "ngRoute", "ui.bootstrap", "ngSanitize"])
// CSRF cookie config
.config(function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
})

// Configuration for Restangular, the Angular REST service
.config(['RestangularProvider', function(RestangularProvider) {
    RestangularProvider.setBaseUrl("/api/v1");
    RestangularProvider.setRequestSuffix("/");
    RestangularProvider.setResponseExtractor(function(response, operation, what, url, httpresp) {
        var newResponse;
        if (operation === "getList") {
            newResponse = response.objects;
            newResponse.metadata = response.meta;
        } else {
            newResponse = response;
        }
        return newResponse;
    });
}])
/*
=== REST Services ===
*/

// Medium REST service
.factory('Medium', ['Restangular', function(Restangular){
    var Medium = Restangular.all('medium');
    return Medium;
}])

// Anbieter REST service
.factory('Anbieter', ['Restangular', function(Restangular){
    var Anbieter = Restangular.all('anbieter');
    return Anbieter;
}])

// Kunde REST service
.factory('Kunde', ['Restangular', function(Restangular){
    var Kunde = Restangular.all('kunde');
    return Kunde;
}])

// Kunde REST service
.factory('Medienart', ['Restangular', function(Restangular){
    var Medienart = Restangular.all('medienart');
    return Medienart;
}])

// Kunde REST service
.factory('Systematik', ['Restangular', function(Restangular){
    var Systematik = Restangular.all('systematik');
    return Systematik;
}])


/*
=== ROUTING ===
*/
.config(["$routeProvider", "djangoStatic", function($routeProvider, djangoStatic) {
    $routeProvider.
        when("/:kundeSlug?/search", {
            templateUrl: djangoStatic("omnds/partials/search.html"),
            controller: "SearchCtrl",  
            title: "Suche"
        }).
        when("/:kundeSlug?", {
            templateUrl: djangoStatic("omnds/partials/main.html"),
            controller: "SplashCtrl",  
            title: "Home"
        });
}])
// Set title based on route
.run(["$rootScope", "$route", function($rootScope, $route) {
    $rootScope.$on("$routeChangeSuccess", function(currentRoute, previousRoute) {
        $rootScope.title = $route.current.title;
    });
}])

// Erzeugt die Such-Box
.directive("mediaSearch", ["djangoStatic", function(djangoStatic) {
    return {
        templateUrl: djangoStatic("omnds/partials/media_search.html"),
        controller: "SearchBoxCtrl",
        scope: {
            kunde: "=",
        }
    }
}])

// Haupt-Controller
.controller("MainCtrl", ["$scope", "$routeParams", "Kunde", function($scope, $routeParams, Kunde) {
    function loadKunde(slug) {
        if(!slug)
        {
            $scope.kunde = null;
            $scope.kundePromise = null;
        }
        else if(!$scope.kunde || ($scope.kunde.slug != slug)) // nur laden bei neuem slug
            $scope.kundePromise = Kunde.get(slug).then(function(data) {
                $scope.kunde = data;
                $scope.kundeSlug = slug;
            });
    }
    $scope.loadKunde = loadKunde;
}])
// Controller für Startseite
.controller("SplashCtrl", ["$scope", "$routeParams", "Kunde", function($scope, $routeParams, Kunde) {
    $scope.kundeSlug = $routeParams.kundeSlug;

    // Kunden / MZ laden
    $scope.loadKunde($scope.kundeSlug);
}])

// Controller für Suchergebnis-Seite
.controller("SearchCtrl", ["$scope", "$routeParams", "$location", "Medium", "Anbieter", "Kunde", "$uibModal", "djangoStatic",  function($scope, $routeParams, $location, Medium, Anbieter, Kunde, $uibModal, djangoStatic) {
    var term = $routeParams.q;

    var medienart = $routeParams.medienart;
    if(medienart && !$.isArray(medienart))
        medienart = [medienart];

    var systematik = $routeParams.systematik;
    if(systematik && !$.isArray(systematik))
        systematik = [systematik];

    var anbieter_id = $routeParams.anbieter;
    $scope.kundeSlug = $routeParams.kundeSlug;
    var kundeSlug = $scope.kundeSlug;
    $scope.searchTerm = term;
    $scope.searchLoading = true;
    $scope.filterMedienart = medienart;
    $scope.filterSystematik = systematik;

    // Kunden / MZ laden
    $scope.loadKunde($scope.kundeSlug);


    function getAnbieter() {
        var filter = {};
        if(kundeSlug)
            filter.kunde = kundeSlug;
        else
            filter.kunde__isnull = true;
    
        return Anbieter.getList(filter).then(function(data) {
            $scope.anbieter = data;
            if(data.length)
                if(anbieter_id) {
                    for(var i=0;i<data.length;i++)
                    {
                        if(data[i].id == anbieter_id)
                        {       
                            $scope.selectedAnbieter = data[i];
                            return;
                        }
                    }
                    $scope.selectedAnbieter = data[0];
                }
                else
                    $scope.selectedAnbieter = data[0];
        });
    }

    function execSearch(offset) {
        $scope.searchLoading = true;
        if(arguments.length < 1)
            offset = 0;

        return Medium.all("search").getList({q: term, anbieter:$scope.selectedAnbieter.id, systematik:systematik, medienart:medienart, offset: offset}).then(function(data) {
            $scope.searchLoading = false;
            if(!$scope.searchResult)
                $scope.searchResult = [];
            angular.forEach(data, function(val) {
                $scope.searchResult.push(val);
            });
            $scope.searchMeta = data.metadata;
            $scope.moreAvailable = (data.length == 20);
        });
    }
    function selectAnbieter(ab) {
        $location.search({q: term, anbieter:ab.id});
    }
    function search(term, medienart, systematik) {
        var query = {q: term, anbieter:$scope.selectedAnbieter.id};

        if(medienart)
            query["medienart"] = medienart;
        if(systematik)
            query["systematik"] = systematik;

        $location.search(query);
    }
    function loadMore() {
        execSearch($scope.searchResult.length);
    }

    function showFilters() {
        var dialog = $uibModal.open({
            templateUrl: djangoStatic("omnds/partials/filter_modal.html"),
            scope: $scope,
            controller: "FilterCtrl",
        });
    }

    function filterResult(filters) {
        var medienart = [];
        var systematik = [];
        angular.forEach(filters.medienart, function(val, key) {
            if(val) medienart.push(key);
        });
        angular.forEach(filters.systematik, function(val, key) {
            if(val) systematik.push(key);
        });
        search(term, medienart, systematik);
        return true;
    }

    getAnbieter().then(execSearch);
    $scope.selectAnbieter = selectAnbieter;
    $scope.search = search;
    $scope.loadMore = loadMore;
    $scope.showFilters = showFilters;
    $scope.filterResult = filterResult;

    $scope.filter_ma = {};
    $scope.filter_sys = {};
    
    angular.forEach(medienart, function(val) {
        $scope.filter_ma[val] = true;
    });
    angular.forEach(systematik, function(val) {
        $scope.filter_sys[val] = true;
    });

}])
// Controller für den Filter-Dialog
.controller("FilterCtrl", ["$scope", "Systematik", "Medienart", function($scope, Systematik, Medienart) {
    $scope.sysdict = {}; // Dictionary nach Systematik-ID
    $scope.martdict = {}; // Dictionary nach Medienart-ID

    var facets = $scope.searchMeta.facets;
    // IDs der Facetten extrahieren
    var mart_ids = $.map(facets.fields.medienart, function(val) {
        return val[0];
    });
    var sys_ids = $.map(facets.fields.systematik, function(val) {
        return val[0];
    });

    // Medienarten vom Server holen
    $scope.loadingMedienart = true;
    Medienart.getList({id__in: mart_ids, limit:0}).then(function(data) {
        angular.forEach(data, function(val) {
            $scope.martdict[val.id] = val;
        });
        $scope.loadingMedienart = false;
    });

    $scope.loadingSystematik = true;
    Systematik.getList({id__in: sys_ids, limit:0}).then(function(data) {
        angular.forEach(data, function(val) {
            $scope.sysdict[val.id] = val;
        });
        $scope.loadingSystematik = false;
    });

}])
// Controller für die Suchbox-Direktive
.controller("SearchBoxCtrl", ["$scope", "$location", function($scope, $location) {
    function search(term) {
        var path = "/search";
        if($scope.kunde) {
            path = "/"+$scope.kunde+"/search";
        }
        $location.path(path).search("q", term);
    }
    $scope.search = search;
}]);
