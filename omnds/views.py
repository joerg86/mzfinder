from django.shortcuts import render
from django.views.generic import TemplateView
from django.conf import settings

# Create your views here.
class AppView(TemplateView):
    template_name = "omnds/app.html"

    def get_context_data(self, **kwargs):
        context = super(AppView, self).get_context_data(**kwargs)
        context['STATIC_URL'] = settings.STATIC_URL
        return context

